﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CameraController : MonoBehaviour
{
    Vector3 ogPos;
    Animator animator;
    private void Awake()
    {
        ogPos = transform.position;
        animator = GetComponent<Animator>();
    }

    public void Punch()
    {
        animator.SetTrigger("Triggered");
    }
    public void Shake()
    {
        StartCoroutine(ScreenShakeRoutine());
    }
    IEnumerator ScreenShakeRoutine()
    {
        float magnitude = 0.05f;
        for (int i = 0; i < 10; i++)
        {
            Vector3 offset = new Vector3(Random.Range(-magnitude, magnitude), Random.Range(-magnitude, magnitude), Random.Range(-magnitude, magnitude));
            transform.position = ogPos + offset;
            yield return null;
        }
        transform.position = ogPos;
    }


}
