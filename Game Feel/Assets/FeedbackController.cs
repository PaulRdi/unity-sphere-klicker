﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackController : MonoBehaviour
{
    Animator theAnimator;
    Material jiggleMat;
    Renderer renderer;
    Camera cam;
    private void Awake()
    {
        theAnimator = GetComponent<Animator>();
        jiggleMat = GetComponent<MeshRenderer>().material;
        renderer = GetComponent<Renderer>();
        cam = camera.GetComponent<Camera>();
    }
    public bool points;
    public bool growAndShrink;
    public bool jiggle;
    public bool betterJiggle;
    public bool color;
    public bool cameraBGColor;
    public bool particles;
    public bool morePoints;
    public bool textCoroutine;
    public bool cameraPunch;
    public bool audio;
    /// <summary>
    /// Its bait!
    /// </summary>
    public bool screenShake;
    public bool environment;
    public bool confetti1, confetti2;

    float pointcounter = 0;
    float lastpointamount = 0;
    float pointMultiplier = 2;

    [Range(0, 0.08f)]    
    public float jiggleAmount;
    [SerializeField] ParticleSystem sphereParticles, confetti1sys, confetti2sys;
    [SerializeField] TMPro.TMP_Text text;
    [SerializeField] CameraController camera;
    [SerializeField] Material coloredMat;
    [SerializeField] Material defaultMat;
    [SerializeField] GameObject environmentObj;
    [SerializeField] Color camCol;
    [SerializeField] AudioClip klickAudio;
    [SerializeField] AudioSource audioSource;
    Coroutine textRoutine;
    float targetPointAmount;

    private void Update()
    {
        jiggleMat.SetFloat("_Amount", jiggleAmount);

        
    }

    public void OnMouseDown()
    {
        pointcounter++;
        if (growAndShrink)
            theAnimator.SetLayerWeight(1, 1.0f);
        else
            theAnimator.SetLayerWeight(1, 0.0f);

        if (jiggle)
            theAnimator.SetLayerWeight(2, 1.0f);
        else
            theAnimator.SetLayerWeight(2, 0.0f);

        if (color)
        {
            renderer.sharedMaterial = coloredMat;
            jiggleMat = renderer.sharedMaterial;
            theAnimator.SetLayerWeight(3, 1.0f);
        }
        else
        {
            renderer.sharedMaterial = defaultMat;
            jiggleMat = renderer.sharedMaterial;
            theAnimator.SetLayerWeight(3, 0.0f);
        }

        if (betterJiggle)
            theAnimator.SetLayerWeight(4, 1.0f);
        else
            theAnimator.SetLayerWeight(4, 0.0f);

        if (points && !morePoints)
            targetPointAmount = pointcounter;
        else if (morePoints)
            targetPointAmount = CalculateBigPoints(pointcounter);
        else if (!morePoints && !points)
            text.text = "";

        if (points || morePoints)
            text.text = targetPointAmount.ToString();

        if (textCoroutine)
            HandleTextRoutine(lastpointamount);
        else
        {
            if (textRoutine != null)
                StopCoroutine(textRoutine);
            textRoutine = null;
        }
        if (particles)
            sphereParticles.Play();

        if (cameraPunch)
            camera.Punch();

        if (screenShake)
            camera.Shake();

        if (confetti1)
            confetti1sys.Play();

        if (confetti2)
            confetti2sys.Play();

        if (cameraBGColor)
            cam.backgroundColor = camCol;
        else
            cam.backgroundColor = Color.black;

        if (audio)
            audioSource.PlayOneShot(klickAudio);

        environmentObj.SetActive(environment);
        theAnimator.SetTrigger("Triggered");
        lastpointamount++;
    }

    private float CalculateBigPoints(float amount)
    {
        return (amount * (amount + pointMultiplier)) * pointMultiplier * 1000;
    }

    private void HandleTextRoutine(float initAmount)
    {
        if (textRoutine == null)
            textRoutine = StartCoroutine(TextRoutine(initAmount));


    }

    IEnumerator TextRoutine(float initAmount)
    {
        float lastShowedValue = CalculateBigPoints(initAmount);
        float targetValue = CalculateBigPoints(pointcounter);

        while (lastShowedValue < targetValue)
        {
            lastShowedValue = Mathf.Ceil(Mathf.Lerp(lastShowedValue, targetValue, 0.05f));
            targetValue = CalculateBigPoints(pointcounter);
            text.text = lastShowedValue.ToString();
            yield return null;
        }
        textRoutine = null;

    }
}
